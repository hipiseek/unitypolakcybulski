﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TargetController : MonoBehaviour
{
    public GameObject levelManager;
    public GameObject levelComplete;
    public GameObject ui;

    private int levelCounter;

    private Button fireButton;
    private GameObject[] respawns;
    private Text currentLevel;
    private int numberOfLevels = 5;
    private GameObject finishGame;

    public void Start()
    {
        levelCounter = 0;
        fireButton = ui.transform.GetChild(0).gameObject.GetComponent<Button>();
        currentLevel = ui.transform.GetChild(5).gameObject.GetComponent<Text>();
        currentLevel.text = "Level: " + (levelCounter + 1);

        finishGame = ui.transform.Find("FinishGame").gameObject;
    }

    public IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        respawns = GameObject.FindGameObjectsWithTag("Cannonball");

        if (other.gameObject.tag =="Cannonball")
        {
            fireButton.interactable = false;

            if(levelCounter>0)
                levelManager.transform.GetChild(levelCounter-1).gameObject.SetActive(false);

            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;

            foreach (var respawn in respawns)
            {
                Destroy(respawn);
            }

            var lvlCompleteText = Instantiate(levelComplete, ui.transform) as GameObject;

            yield return new WaitForSeconds(3);

            if (levelCounter == numberOfLevels)
            {
                finishGame.SetActive(true);
                yield break;
            }

            levelManager.transform.GetChild(levelCounter).gameObject.SetActive(true);

            levelCounter++;

            currentLevel.text = "Level: " + (levelCounter+1);

            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            gameObject.GetComponent<CircleCollider2D>().enabled = true;


            fireButton.interactable = true;
        }
    }


}
