﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject exitMenu;
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }


    public void ExitToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void HideConfirmation()
    {
        exitMenu.SetActive(false);
    }

    public void ShowConfirmation()
    {
        exitMenu.SetActive(true);
    }
}
