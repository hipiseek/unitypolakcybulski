﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PointAndShoot : MonoBehaviour
{
    public GameObject player;
    public GameObject bulletPrefab;
    public GameObject bulletStart;
    public GameObject target;
    public float bulletSpeed;

    private Vector2 direction;
    private Vector3 difference;

    private List<GameObject> listOfCannonballs;

    public Text currentBall;


    public void OnValueChanged(float value)
    {
        bulletSpeed = value;
    }

    private void Start()
    {
        listOfCannonballs = Resources.LoadAll<GameObject>("Prefabs/Cannonballs").ToList();
        bulletSpeed = 10;
    }

    public void fireBullet()
    {
        difference = target.transform.position - player.transform.position;

        direction = difference.normalized;
        

        GameObject b = Instantiate(bulletPrefab) as GameObject;
        b.transform.position = bulletStart.transform.position;
        b.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        
    }

    public void ChangeBall()
    {
        var currentBallName = currentBall.text.ToString();
        bulletPrefab = listOfCannonballs.Where(x=>x.name==currentBallName).FirstOrDefault();
    }

}
