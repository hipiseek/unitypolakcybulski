﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customize : MonoBehaviour
{
    public SpriteRenderer rend;
    Object[] listOfSprites;
    public Text ballName;
    private int currentSkin =0;
    private int amountOfSkins;

    void Start()
    { 
        listOfSprites = Resources.LoadAll("Ball", typeof(Sprite));
        amountOfSkins = listOfSprites.Length;
        ballName.text = rend.sprite.name;
    }

    // Update is called once per frame
    
    public void NextSkin()
    {
        currentSkin++;
        if (currentSkin >= amountOfSkins)
            currentSkin = 0;
        rend.sprite = listOfSprites[currentSkin] as Sprite;
        ballName.text = rend.sprite.name;
      
    }

    public void PreviousSkin()
    {
        currentSkin--;
        if (currentSkin < 0)
            currentSkin = amountOfSkins - 1;
        rend.sprite = listOfSprites[currentSkin] as Sprite;
        ballName.text = rend.sprite.name;
        
    }

}
